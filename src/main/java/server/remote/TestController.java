package server.remote;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping(value = "/")
    String getMessage() {
        return "Hello world!!";
    }

    @GetMapping(value = "/")
    String getMessageWithBody(@RequestHeader("token") String token, @RequestBody TestController body) {
        return "Hello world!!";
    }

}
