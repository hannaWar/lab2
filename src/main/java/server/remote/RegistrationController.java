package server.remote;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import server.models.TokenResponse;

import java.util.Arrays;
import java.util.Random;

@RestController
public class RegistrationController {
    @GetMapping("/token")
    TokenResponse getToken(){
        TokenResponse value=new TokenResponse(String.format("%x", Arrays.hashCode(("" + new Random().nextLong()).getBytes())));
        return value;
    }
}
