package server.remote;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import server.models.ShipInformationResponse;

@RestController
public class ShipInformationController {
    @GetMapping(value = "/ship")
    ShipInformationResponse getShipInformationWithBody(@RequestHeader("token") String token, @RequestBody ShipInformationController body) {
        return new ShipInformationResponse();
    }
}
