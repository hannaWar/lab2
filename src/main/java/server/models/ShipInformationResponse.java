package server.models;

public class ShipInformationResponse {
    private boolean priority;
    private boolean violation;

    public ShipInformationResponse(boolean priority, boolean violation) {
        this.priority = priority;
        this.violation = violation;
    }

    public ShipInformationResponse(){

    }

    public boolean isPriority() {
        return priority;
    }

    public void setPriority(boolean priority) {
        this.priority = priority;
    }

    public boolean isViolation() {
        return violation;
    }

    public void setViolation(boolean violation) {
        this.violation = violation;
    }
}
