package server.remote;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import server.models.LoadingResponse;

import java.util.Random;

@RestController
public class DataExchangeController {
    @GetMapping("/loading")
    LoadingResponse getLoading() {
        return new LoadingResponse(new Random().nextBoolean());
    }

    @PutMapping("/loading")
    LoadingResponse putLoading(boolean value) {
        return new LoadingResponse(value);
    }
}
