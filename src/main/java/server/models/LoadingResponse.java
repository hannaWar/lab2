package server.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoadingResponse {
    @JsonProperty("loading")
    private boolean loading;

    public LoadingResponse(){

    }

    public LoadingResponse(boolean loading) {
        this.loading = loading;
    }

    public boolean getLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }
}
